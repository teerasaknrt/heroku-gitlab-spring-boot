package com.example.demo.service;

import org.springframework.stereotype.Service;
import com.example.demo.entiry.Person;

@Service
public class PersonService
{
    public Person viewPerson()
    {
        return new Person("Spring-Boot","On Heroku TEST CI/CD!");
    }
}